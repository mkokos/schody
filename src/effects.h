#include <Arduino.h>
#include <Adafruit_NeoPixel.h>
uint32_t Wheel(Adafruit_NeoPixel &pixels,byte WheelPos);
void colorWipe(Adafruit_NeoPixel &pixels,uint32_t c, uint8_t wait) ;
void rainbow(Adafruit_NeoPixel &pixels,uint8_t wait) ;
void rainbowCycle(Adafruit_NeoPixel &pixels,uint8_t wait);
void theaterChase(Adafruit_NeoPixel &pixels,uint32_t c, uint8_t wait);
void theaterChaseRainbow(Adafruit_NeoPixel &pixels,uint8_t wait)